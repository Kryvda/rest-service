package com.example.restservice;

import java.io.File;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class ExecutorController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    static int n = 100;

    @GetMapping("/greeting")
    public Executor greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Executor(n, String.format(template, name));
    }

    @GetMapping("/count")
    public Executor count(@RequestParam(value = "name", defaultValue = "Main.java") String pattern) {
        return new Executor(counter.incrementAndGet(), new ForkJoinPool().invoke(new FS(new File("C:\\Users\\Гість\\IdeaProjects\\untitled1\\gs-rest-service\\initial"), 2, pattern)) + " - " + pattern);

    }


    @PostMapping(value = "/greetings")
    public String greetings(@RequestParam(value = "name", defaultValue = "World") String greeting) {
        n = n * 100;
        return "nice";
    }
    @DeleteMapping("/files/{id}")
    String deleteFile(@PathVariable String id) {

        BinaryFile.delete(id);                                                              
        return id;
    }
	@PutMapping(value = "files/create/{content}")
	public String Create(@RequestParameter(value = "content", defaultValue = = "")String content){
		try{
			BinaryFile.Create("new file", content);
		}
		catch(Excption e){
			return e.getMessage();
		}
		return "Файл створено";                  
	}
	@Test
	public void TestCount(){
		count("some file");
	}
	@Test
	public void TestDelete(){
		deleteFile("some file");
	}
	@Test
	public void TestCreate(){
		deleteFile("some file");
	}
}
